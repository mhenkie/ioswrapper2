module CDW
  class Model < NSManagedObject

    def inspect
      properties = []
      entity.properties.select { |p| p.is_a?(NSAttributeDescription) }.each do |property|
        properties << "#{property.name}: #{valueForKey(property.name).inspect}"
      end

      "#<#{entity.name} #{properties.join(", ")}>"
    end

    def self.new context=Store.managedObjectContext
      alloc.initWithEntity(entity, insertIntoManagedObjectContext:context)
    end

    def self.create attributes={}, context=Store.managedObjectContext
      model = alloc.initWithEntity(entity, insertIntoManagedObjectContext:context)
      attributes.each do |keyPath, value|
        model.setValue(value, forKey:keyPath)
      end
      model
    end

    def self.entity
      @entity ||= Store.managedObjectModel.entitiesByName[name]
    end

    def self.all 
      fetcher.all
    end

    def self.first 
      fetcher.first
    end

    def self.last
      fetcher.last
    end

    def self.limit(l)
      fetcher.limit(l)
    end

    def self.offset(o)
      fetcher.offset(o)
    end 

    def self.order(*args)
      fetcher.order(*args)
    end

    def self.where(*args)
      fetcher.where(*args)
    end  

    def self.fetcher
      fetchRequest = Fetcher.alloc.init
      fetchRequest.setEntity entity
      fetchRequest
    end

    def self.count context=Store.managedObjectContext
      request = NSFetchRequest.alloc.init
      request.setEntity entity
      request.setIncludesSubentities false # Omit subentities. Default is YES (i.e. include subentities)

      error = Pointer.new(:object)
      count = context.countForFetchRequest request, error:error
      if count == NSNotFound 
        raise error[0] if error[0]
      end
      return count
    end

    def self.truncate context=Store.managedObjectContext
      request = NSFetchRequest.alloc.init
      request.setEntity entity
      request.setIncludesPropertyValues false 
      request.setIncludesSubentities false

      error = Pointer.new(:object)
      objects = context.executeFetchRequest request, error:error
      
      if objects.nil?
        puts error[0] if error[0]
      else
        objects.each do |object|
          context.deleteObject object
        end
        context.save nil 
      end
    end

  end
end