module CDW
  class Fetcher < NSFetchRequest

    def inspect 
      fetch
    end

    def fetch
      error_ptr = Pointer.new(:object)
      Store.managedObjectContext.executeFetchRequest(self, error:error_ptr)
    end

    def count
      return fetch.count if fetchOffset > 0
      old_result_type = self.resultType
      self.resultType = NSCountResultType
      count = fetch[0]
      self.resultType = old_result_type
      return count
    end

    def all
      fetch
    end

    def first 
      limit(1).fetch[0]
    end

    def last
      self.fetchOffset = self.count - 1 unless self.count < 1
      limit(1).fetch[0]
    end

    def offset o
      o = o.to_i
      raise ArgumentError, "offset '#{o}' cannot be less than zero." if o < 0
      self.fetchOffset = o
      self
    end

    def limit l
      l = l.to_i
      raise ArgumentError, "limit '#{l}' cannot be less than zero. Use zero for no limit." if l < 0
      self.fetchLimit = l
      self
    end

    def order key, ascending=true 
      descriptors = sortDescriptors.nil? ? [] : sortDescriptors.mutableCopy
      descriptors << NSSortDescriptor.sortDescriptorWithKey(key.to_s, ascending:ascending)
      self.sortDescriptors = descriptors
      self
    end

    def where format, *args
      new_predicate = NSPredicate.predicateWithFormat(format, argumentArray:args)

      if self.predicate
        self.predicate = NSCompoundPredicate.andPredicateWithSubpredicates([predicate, new_predicate])
      else
        self.predicate = new_predicate
      end

      self
    end

  end
end