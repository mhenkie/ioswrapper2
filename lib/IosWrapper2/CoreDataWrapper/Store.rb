module CDW
  module Store 

    def self.managedObjectContext 
      @managedObjectContext ||= begin
        coordinator = self.persistentStoreCoordinator
        if coordinator != nil 
          context = NSManagedObjectContext.alloc.initWithConcurrencyType NSMainQueueConcurrencyType
          context.setPersistentStoreCoordinator coordinator
        end
       context
     end
    end
     
    def self.managedObjectModel 
      @managedObjectModel ||= begin
        model = NSManagedObjectModel.mergedModelFromBundles([NSBundle.mainBundle]).mutableCopy
        model.entities.each do |entity|
          begin
            Kernel.const_get(entity.name)
            entity.setManagedObjectClassName(entity.name)
          rescue NameError
            entity.setManagedObjectClassName("Model")
          end
        end

        model
      end
    end

    def self.persistentStoreCoordinator 
      @persistentStoreCoordinator ||= begin
        libPath    = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, true)[0]
        storeUrl    = NSURL.fileURLWithPath libPath.stringByAppendingPathComponent "db.sqlite"
        error       = Pointer.new(:object)
        coordinator = NSPersistentStoreCoordinator.alloc.initWithManagedObjectModel self.managedObjectModel
        options     = { NSMigratePersistentStoresAutomaticallyOption => true, NSInferMappingModelAutomaticallyOption => true }
        if !coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration:nil, URL:storeUrl, options:options, error:error) 
          raise error[0].localizedDescription if error[0]
        end
        coordinator
      end
    end

    def self.save error=nil
      managedObjectContext.save error
    end

    def self.delete object 
      managedObjectContext.deleteObject object
    end

  end
end