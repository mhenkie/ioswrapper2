module Delegate 

  def delegate=(delegate)
    @delegate = WeakRef.new(delegate)
  end

  def delegate 
    @delegate
  end

end