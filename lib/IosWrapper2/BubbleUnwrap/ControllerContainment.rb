module ControllerContainment 

  def displayContentController controller
    @activeController = controller
    self.addChildViewController controller  
    controller.view.frame = frameForContentController
    self.view.addSubview controller.view
    controller.didMoveToParentViewController self  
  end  

  def hideContentController content
    return if not @activeController 
    @activeController.willMoveToParentViewController nil
    @activeController.view.removeFromSuperview        
    @activeController.removeFromParentViewController  
    @activeController = nil  
  end

  # transition from old to new controller
  def cycleToViewController(newC, reverse = false)
    if @activeController
      oldC = @activeController
      @activeController = newC
      oldC.willMoveToParentViewController nil                    
      self.addChildViewController newC

      dasFrame = frameForContentController
      width    = dasFrame.size.width 
      height   = dasFrame.size.height 
      yOffset  = dasFrame.origin.y
      newFrame = CGRectMake(reverse ? -width : width, yOffset, width, height)
      newC.view.frame = newFrame
      newC.view.alpha = 0.3               
      end_frame = CGRectMake(reverse ? width : -width, yOffset, width, height)

      anim = -> {
        newC.view.frame = dasFrame
        newC.view.alpha = 1.0                     
        oldC.view.frame = end_frame
        oldC.view.alpha = 0.3
      }
      complete = -> finished {
        oldC.removeFromParentViewController  
        oldC.view.removeFromSuperview                
        newC.didMoveToParentViewController self
        @isTransitioning = false
      }

      self.transitionFromViewController(oldC, toViewController:newC, duration:0.4, options:UIViewAnimationOptionCurveEaseInOut, animations:anim, completion:complete)
      @isTransitioning = true
    else
      displayContentController newC
    end
  end

  def frameForContentController 
    size = UIScreen.mainScreen.bounds.size 
    orientation = UIApplication.sharedApplication.statusBarOrientation
    portrait = orientation == UIInterfaceOrientationPortrait ||
               orientation == UIInterfaceOrientationPortraitUpsideDown
    width  = portrait ? size.width  : size.height
    height = portrait ? size.height : size.width

    frame = CGRectMake(0, 0, width, height)
  end

  def frameForContentController 
    size = UIScreen.mainScreen.bounds.size 
    if SystemVersion > "7.1"
      frame = CGRectMake(0, 0, size.width, size.height)
    else 
      orientation = UIApplication.sharedApplication.statusBarOrientation
      portrait = orientation == UIInterfaceOrientationPortrait ||
                 orientation == UIInterfaceOrientationPortraitUpsideDown
      width  = portrait ? size.width  : size.height
      height = portrait ? size.height : size.width
      frame = CGRectMake(0, 0, width, height)
    end
    frame
  end

end