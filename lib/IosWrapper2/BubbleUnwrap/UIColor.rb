class UIColor 

	def lighterColor 
    h = Pointer.new :float  
    s = Pointer.new :float  
    b = Pointer.new :float  
    a = Pointer.new :float 

    if self.getHue h, saturation:s, brightness:b, alpha:a
        return  UIColor.colorWithHue h[0],
                         saturation: s[0],
                         brightness: [b[0] * 1.3, 1.0].min,
                              alpha: a[0]
    end
    return self
  end

  def darkerColor 
    h = Pointer.new :float  
    s = Pointer.new :float  
    b = Pointer.new :float  
    a = Pointer.new :float 

    if self.getHue h, saturation:s, brightness:b, alpha:a
        return  UIColor.colorWithHue h[0],
                         saturation: s[0],
                         brightness: b[0] * 0.75,
                              alpha: a[0]
    end
    return self
  end
  
end