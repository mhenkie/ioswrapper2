module SystemVersion 

  module_function

  def ==(v)
    UIDevice.currentDevice.systemVersion.compare(v, options:NSNumericSearch) == NSOrderedSame
  end

  def <(v) 
    UIDevice.currentDevice.systemVersion.compare(v, options:NSNumericSearch) == NSOrderedAscending
  end

  def >(v) 
    UIDevice.currentDevice.systemVersion.compare(v, options:NSNumericSearch) == NSOrderedDescending
  end

end