module KeyChain

  module_function 

  def serviceName 
    NSBundle.mainBundle.bundleIdentifier
  end

  def newSearchDictionary identifier
    searchDictionary = NSMutableDictionary.alloc.init
    searchDictionary.setObject KSecClassGenericPassword, forKey:KSecClass
    encodedIdentifier = identifier.dataUsingEncoding NSUTF8StringEncoding
    searchDictionary.setObject encodedIdentifier, forKey:KSecAttrGeneric
    searchDictionary.setObject encodedIdentifier, forKey:KSecAttrAccount
    searchDictionary.setObject serviceName, forKey:KSecAttrService
    return searchDictionary
  end

  def search identifier 
    searchDictionary = newSearchDictionary identifier
    searchDictionary.setObject KSecMatchLimitOne, forKey:KSecMatchLimit
    searchDictionary.setObject KCFBooleanTrue, forKey:KSecReturnData
    result = Pointer.new :object
    status = SecItemCopyMatching(searchDictionary, result)
    if result[0]
      return result[0].to_s 
    end
    return nil 
  end

  def create item, forIdentifier:identifier
    dictionary = newSearchDictionary identifier
    passwordData = item.dataUsingEncoding NSUTF8StringEncoding
    dictionary.setObject passwordData, forKey:KSecValueData
    status = SecItemAdd(dictionary, nil)
    if status == ErrSecSuccess
      return true
    end
    return true
  end

  def update item, forIdentifier:identifier
    searchDictionary = newSearchDictionary identifier
    updateDictionary = NSMutableDictionary.alloc.init
    passwordData = item.dataUsingEncoding NSUTF8StringEncoding
    updateDictionary.setObject passwordData, forKey:KSecValueData
    status = SecItemUpdate(searchDictionary, updateDictionary)
    if status == ErrSecSuccess 
      return true
    end
    return false
  end

  def delete identifier
    searchDictionary = newSearchDictionary identifier
    SecItemDelete(searchDictionary)
  end

end