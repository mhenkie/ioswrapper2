# encoding: utf-8

$:.unshift File.expand_path('../lib', __FILE__)
require 'IosWrapper2/version'

Gem::Specification.new do |s|
  s.name          = "ioswrapper2"
  s.version       = IosWrapper2::VERSION
  s.authors       = ["Noxqs"]
  s.email         = ["noxqslabs@gmail.com"]
  s.homepage      = "http://www.noxqs.nl"
  s.summary       = "Summary"
  s.description   = "Description"
  s.license       = "POS"

  s.files         = Dir["lib/**/*"] + ["README.md"]
  s.test_files    = Dir["spec/**/*"]
  s.platform      = Gem::Platform::RUBY
  s.require_paths = ['lib']

  s.add_development_dependency "rake"
end
